package com.she.blew.me.constants;

/**
 * Created by dam on 28/6/18.
 */
public class Global {
    public static final int MAX_NODES = 2048;

    public static class Message {
        public static final String IP = "ip";
        public static final String PORT = "port";
        public static final String MESSAGE_TYPE = "messageType";

        public static class BroadcastMessage {
            public static final String MESSAGE_TYPE = "broadcastMessage";
        }
        public static class AddNodeMessage {
            public static final String MESSAGE_TYPE = "addNodeMessage";
        }
    }
}
