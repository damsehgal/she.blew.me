package com.she.blew.me.util;

import com.she.blew.me.model.Message.Message;
import com.she.blew.me.model.Node;

/**
 * Created by dam on 27/6/18.
 */
public class SendMessage {
    public static void sendMessage(Node sender, Node receiver, Message message) {
        sender.send(message, receiver);
    }
}
