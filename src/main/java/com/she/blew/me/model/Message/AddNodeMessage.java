package com.she.blew.me.model.Message;

import com.she.blew.me.constants.Global;

public class AddNodeMessage extends BroadcastMessage {

    public AddNodeMessage(String ip, int port) {
        this.header.put(Global.Message.IP, ip);
        this.header.put(Global.Message.PORT, String.valueOf(port));
        this.header.put(Global.Message.MESSAGE_TYPE,
                Global.Message.AddNodeMessage.MESSAGE_TYPE);
    }
}
