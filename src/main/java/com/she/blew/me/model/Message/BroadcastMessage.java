package com.she.blew.me.model.Message;

import com.she.blew.me.constants.Global;

/**
 * Created by dam on 27/6/18.
 */
public class BroadcastMessage extends Message {
    public BroadcastMessage() {
        broadcast = true;
        this.header.put(Global.Message.MESSAGE_TYPE,
                Global.Message.BroadcastMessage.MESSAGE_TYPE);
    }
}