package com.she.blew.me.model.Message;

import java.util.Map;

/**
 * Created by dam on 27/6/18.
 */
public class Message {
    protected Map<String, String> header;
    protected Map<String, String> properties;
    protected String body;
    protected boolean broadcast = false;
    protected boolean acknowledge = false;

    public boolean isAcknowledge() {
        return acknowledge;
    }

    public boolean isBroadcast() {
        return broadcast;
    }

    public Map<String, String> getHeader() {
        return header;
    }

    public void setHeader(Map<String, String> header) {
        this.header = header;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
