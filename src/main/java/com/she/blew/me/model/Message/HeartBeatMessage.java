package com.she.blew.me.model.Message;

/**
 * Created by dam on 27/6/18.
 */
public class HeartBeatMessage extends Message {
    public int delayTime;

    public int getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(int delayTime) {
        this.delayTime = delayTime;
    }
}
