package com.she.blew.me.model;

import com.she.blew.me.model.Message.AddNodeMessage;
import com.she.blew.me.model.Message.BroadcastMessage;
import com.she.blew.me.model.Message.Message;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by dam on 27/6/18.
 */
public class Node {
    List<Node> neighbors;
    Node successor;
    Node predecessor;

    private String ip;
    private int port;

    public Node(String ip, int port) {
        this.ip = ip;
        this.port = port;
        send(new AddNodeMessage());
    }

    /**
     * sends message only to neighbors
     * @param message
     */
    public void send(Message message) {
        neighbors.forEach(node -> send(message, node));
    }
    /**
     * todo -> send actual message here
     * sends message to receiver if present in neighbors else throws exception
     * @param message
     * @param receiver
     */
    public void send (Message message, Node receiver) {
    }

    public void receive(Message message, Node sender) {
    }

    public int getPort() {
        return port;
    }

    public int getHashCode() {
        // only take first 10 chars -> (2 ^ 4) ^ 10 combinations
        return Integer.parseInt(Arrays.toString(DigestUtils.sha256(ip + port))
                .substring(0, 10).toLowerCase(), 16);
    }

    public void joinCluster() {
        send(new BroadcastMessage());
    }


}