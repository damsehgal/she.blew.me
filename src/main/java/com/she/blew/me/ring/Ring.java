package com.she.blew.me.ring;


import com.she.blew.me.model.Node;

import java.util.ArrayList;
import java.util.List;

// this is happening at application level,
// todo -> remove this spof
public class Ring {
    String ip;
    int port;
    List<Node> currentKnownNodes;
    public Ring() {
        currentKnownNodes = new ArrayList<>();
    }
    public void add(String ip, int port) {
        currentKnownNodes.add(new Node(ip, port));
    }

}
